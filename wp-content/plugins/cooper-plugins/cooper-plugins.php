<?php
/*
Plugin Name: Cooper Plugins
Plugin URI: http://webrdox.net/
Description: Declares a plugin that will create Page Settins, VC Addons & Custom Post Type
Version: 2.0
Author: WebRedox
Author URI: http://webredox.net/
License: GPLv2
*/

include plugin_dir_path( __FILE__ ).'metaboxes.php';

if( ! function_exists( 'portfolio_post_types' ) ) {
    function portfolio_post_types() {

        register_post_type(
            'portfolio',
            array(
                'labels' => array(
                    'name'          => __( 'Portfolio', 'portfolio' ),
                    'singular_name' => __( 'Portfolio', 'portfolio' ),
                    'add_new'       => __( 'Add New', 'portfolio' ),
                    'add_new_item'  => __( 'Add New Portfolio Item', 'portfolio' ),
                    'edit'          => __( 'Edit', 'portfolio' ),
                    'edit_item'     => __( 'Edit Portfolio', 'portfolio' ),
                    'new_item'      => __( 'New Portfolio', 'portfolio' ),
                    'view'          => __( 'View Portfolio', 'portfolio' ),
                    'view_item'     => __( 'View Portfolio', 'portfolio' ),
                    'search_items'  => __( 'Search Portfolio', 'portfolio' ),
                    'not_found'     => __( 'No Portfolio item found', 'portfolio' ),
                    'not_found_in_trash' => __( 'No portfolio item found in Trash', 'portfolio' ),
                    'parent'        => __( 'Parent Portfolio', 'portfolio' ),
                ),
                
                'description'       => __( 'Create a Portfolio.', 'portfolio' ),
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'          => true,
                'publicly_queryable'    => true,
				'capability_type' => 'post',
                'exclude_from_search'   => true,
                'menu_position'         => 6,
                'hierarchical'      => false,
                'query_var'         => true,
				'menu_icon' => 'dashicons-format-gallery',
                'supports'  => array (
                    'title', //Text input field to create a post title.
                    'editor',
                    'thumbnail',
                    
                )
            )
        );
register_taxonomy('portfolio_category', 'portfolio', array('hierarchical' => true, 'label' => 'Categories', 'singular_name' => 'Category', "rewrite" => true, "query_var" => true));         

    }	
}

add_action( 'init', 'portfolio_post_types' ); // register post type

register_taxonomy_for_object_type('category', 'custom-type');

if( ! function_exists( 'services_post_types' ) ) {
    function services_post_types() {

        register_post_type(
            'services',
            array(
                'labels' => array(
                    'name'          => __( 'Services', 'services' ),
                    'singular_name' => __( 'Services', 'services' ),
                    'add_new'       => __( 'Add New', 'services' ),
                    'add_new_item'  => __( 'Add New Services', 'services' ),
                    'edit'          => __( 'Edit', 'services' ),
                    'edit_item'     => __( 'Edit Services', 'services' ),
                    'new_item'      => __( 'New Services', 'services' ),
                    'view'          => __( 'View Services', 'services' ),
                    'view_item'     => __( 'View Services', 'services' ),
                    'search_items'  => __( 'Search Services', 'services' ),
                    'not_found'     => __( 'No Services Item found', 'services' ),
                    'not_found_in_trash' => __( 'No Services item found in Trash', 'services' ),
                    'parent'        => __( 'Parent Services', 'services' ),
                ),
                
                'description'       => __( 'Create a Services.', 'services' ),
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'          => true,
                'publicly_queryable'    => true,
				'capability_type' => 'post',
                'exclude_from_search'   => true,
                'menu_position'         => 6,
                'hierarchical'      => false,
                'query_var'         => true,
				'menu_icon' => 'dashicons-megaphone',
                'supports'  => array (
                    'title', //Text input field to create a post title.
                    'editor',
                    'thumbnail',
                    
                )
            )
        );
register_taxonomy('services_category', 'services', array('hierarchical' => true, 'label' => 'Categories', 'singular_name' => 'Category', "rewrite" => true, "query_var" => true));

    }
}

add_action( 'init', 'services_post_types' ); // register post type

register_taxonomy_for_object_type('category', 'custom-type');

if( ! function_exists( 'resume_post_types' ) ) {
    function resume_post_types() {

        register_post_type(
            'resume',
            array(
                'labels' => array(
                    'name'          => __( 'Resume', 'resume' ),
                    'singular_name' => __( 'Resume', 'resume' ),
                    'add_new'       => __( 'Add New', 'resume' ),
                    'add_new_item'  => __( 'Add New Resume', 'resume' ),
                    'edit'          => __( 'Edit', 'resume' ),
                    'edit_item'     => __( 'Edit Resume', 'resume' ),
                    'new_item'      => __( 'New Resume', 'resume' ),
                    'view'          => __( 'View Resume', 'resume' ),
                    'view_item'     => __( 'View Resume', 'resume' ),
                    'search_items'  => __( 'Search Resume', 'resume' ),
                    'not_found'     => __( 'No Resume Item found', 'resume' ),
                    'not_found_in_trash' => __( 'No Resume item found in Trash', 'resume' ),
                    'parent'        => __( 'Parent Resume', 'resume' ),
                ),
                
                'description'       => __( 'Create a Resume.', 'resume' ),
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'          => true,
                'publicly_queryable'    => true,
				'capability_type' => 'post',
                'exclude_from_search'   => true,
                'menu_position'         => 6,
                'hierarchical'      => false,
                'query_var'         => true,
				'menu_icon' => 'dashicons-id',
                'supports'  => array (
                    'title', //Text input field to create a post title.
                    'editor',
                    'thumbnail',
                    
                )
            )
        );
register_taxonomy('resume_category', 'resume', array('hierarchical' => true, 'label' => 'Categories', 'singular_name' => 'Category', "rewrite" => true, "query_var" => true));

    }
}

add_action( 'init', 'resume_post_types' ); // register post type

register_taxonomy_for_object_type('category', 'custom-type');

if( ! function_exists( 'testimonials_post_types' ) ) {
    function testimonials_post_types() {

        register_post_type(
            'testimonials',
            array(
                'labels' => array(
                    'name'          => __( 'Testimonials', 'testimonials' ),
                    'singular_name' => __( 'Testimonials', 'testimonials' ),
                    'add_new'       => __( 'Add New', 'testimonials' ),
                    'add_new_item'  => __( 'Add New Testimonials', 'testimonials' ),
                    'edit'          => __( 'Edit', 'testimonials' ),
                    'edit_item'     => __( 'Edit Testimonials', 'testimonials' ),
                    'new_item'      => __( 'New Testimonials', 'testimonials' ),
                    'view'          => __( 'View Testimonials', 'testimonials' ),
                    'view_item'     => __( 'View Testimonials', 'testimonials' ),
                    'search_items'  => __( 'Search Testimonials', 'testimonials' ),
                    'not_found'     => __( 'No Testimonials Item found', 'testimonials' ),
                    'not_found_in_trash' => __( 'No Testimonials item found in Trash', 'testimonials' ),
                    'parent'        => __( 'Parent Testimonials', 'testimonials' ),
                ),
                
                'description'       => __( 'Create a Testimonials.', 'testimonials' ),
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'          => true,
                'publicly_queryable'    => true,
				'capability_type' => 'post',
                'exclude_from_search'   => true,
                'menu_position'         => 6,
                'hierarchical'      => false,
                'query_var'         => true,
				'menu_icon' => 'dashicons-format-quote',
                'supports'  => array (
                    'title', //Text input field to create a post title.
                    'editor',
                    'thumbnail',
                    
                )
            )
        );
register_taxonomy('testimonials_category', 'testimonials', array('hierarchical' => true, 'label' => 'Categories', 'singular_name' => 'Category', "rewrite" => true, "query_var" => true));

    }
}

add_action( 'init', 'testimonials_post_types' ); // register post type

register_taxonomy_for_object_type('category', 'custom-type');

if( ! function_exists( 'slider_post_types' ) ) {
    function slider_post_types() {

        register_post_type(
            'slider',
            array(
                'labels' => array(
                    'name'          => __( 'Slider', 'slider' ),
                    'singular_name' => __( 'Slider', 'slider' ),
                    'add_new'       => __( 'Add New', 'slider' ),
                    'add_new_item'  => __( 'Add New Slide', 'slider' ),
                    'edit'          => __( 'Edit', 'slider' ),
                    'edit_item'     => __( 'Edit Slide', 'slider' ),
                    'new_item'      => __( 'New Slide', 'slider' ),
                    'view'          => __( 'View Slide', 'slider' ),
                    'view_item'     => __( 'View Slide', 'slider' ),
                    'search_items'  => __( 'Search Slider', 'slider' ),
                    'not_found'     => __( 'No Slider Item found', 'slider' ),
                    'not_found_in_trash' => __( 'No Slider item found in Trash', 'slider' ),
                    'parent'        => __( 'Parent Slide', 'slider' ),
                ),
                
                'description'       => __( 'Create a Slide.', 'slider' ),
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'          => true,
                'publicly_queryable'    => true,
				'capability_type' => 'post',
                'exclude_from_search'   => true,
                'menu_position'         => 6,
                'hierarchical'      => false,
                'query_var'         => true,
				'menu_icon' => 'dashicons-images-alt2',
                'supports'  => array (
                    'title', //Text input field to create a post title.
                    'editor',
                    'thumbnail',
                    
                )
            )
        );
register_taxonomy('slider_category', 'slider', array('hierarchical' => true, 'label' => 'Categories', 'singular_name' => 'Category', "rewrite" => true, "query_var" => true));

    }
}

add_action( 'init', 'slider_post_types' ); // register post type

/**
*
*
*
 * Allow shortcodes in widgets
 * @since v1.0
 */
add_filter('widget_text', 'do_shortcode');

if( !function_exists('symple_fix_shortcodes') ) {
	function symple_fix_shortcodes($content){   
		$array = array (
			'<p>['		=> '[', 
			']</p>'		=> ']', 
			']<br />'	=> ']'
		);
		$content = strtr($content, $array);
		return $content;
	}
	add_filter('the_content', 'symple_fix_shortcodes');
}

// Section Title Shortcode (Visual)

if(! function_exists('wr_vc_section_title_shortcode')){
	function wr_vc_section_title_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'title'=>'',
			'subtitle'=>'',
			'margin'=>'',
			'padding'=>'',			
			'color'=>'',
			'font_size'=>'',
			'font_weight'=>'',
			'line_height'=>'',
			'text_align'=>'',
			'text_transform'=>'',
			'float'=>'',
			), $atts) );
		$html='';		
		    $html .='<div class="sec-title '.$class.' '.$float.'">';
				if($subtitle != '') {
				$html .='<h4 class="text-subtitle" style="margin:'.$margin.';padding:'.$padding.'; color:'.$color.'; font-size:'.$font_size.'; font-weight:'.$font_weight.'; line-height:'.$line_height.'; text-align:'.$text_align.'; text-transform:'.$text_transform.';">'.$subtitle.'</h4>';
				} if($title != '') {
				$html .='<div class="text-title">';	
				$html .=''.$title.'';					
				$html .='</div>'; 
				} 	
			$html .='</div>';                
		return $html;

	}
	add_shortcode('wr_vc_section_title', 'wr_vc_section_title_shortcode');
}


// Section Text Shortcode (Visual)

if(! function_exists('wr_vc_section_text_shortcode')){
	function wr_vc_section_text_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'title'=>'',
			'background'=>'',
			'margin'=>'',
			'padding'=>'',			
			'color'=>'',
			'font_size'=>'',
			'font_weight'=>'',
			'line_height'=>'',
			'text_align'=>'',
			'text_transform'=>'',
			), $atts) );

				
		return '
		     <div class="sec-text '.$class.'" style="margin:'.$margin.';padding:'.$padding.'; background:'.$background.';">
			 <p style="color:'.$color.'; font-size:'.$font_size.'; font-weight:'.$font_weight.'; line-height:'.$line_height.'; text-align:'.$text_align.'; text-transform:'.$text_transform.';">'.$content.'</p>
			 </div>
                
				';
	}
	add_shortcode('wr_vc_section_text', 'wr_vc_section_text_shortcode');
}


// Section Image Shortcode (Visual)

if(! function_exists('wr_vc_section_image_shortcode')){
	function wr_vc_section_image_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'width'=>'',
			'height'=>'',
			'margin'=>'',
			'padding'=>'',			
			'position'=>'',			
			'float'=>'',			
			'top'=>'',
			'bottom'=>'',
			'right'=>'',
			'left'=>'',
			'img_url'=>'',
			'link_url'=>'',
			'link_target'=>'',			
			'img_popup'=>'',
			'featyretype'=>'',

			), $atts) );

		$html='';
			
            $cooper_back_image ="";
            if($img_url != '' || $img_url != ' ') { 
	            $cooper_back_image = wp_get_attachment_image_src( $img_url, 'full');
            }
				
		    if($featyretype == "st2"){
				$html .='<div class="sec-image post-media box-item vis-det fl-wrap '.$class.'">';
                    $html .='<a data-src="'.$cooper_back_image[0].'" class="image-popup"><i class="fa fa-search"></i></a>';	$html .='<img src="'.$cooper_back_image[0].'" style="width:'.$width.';height:'.$height.';float:'.$float.';margin:'.$margin.';padding:'.$padding.';position:'.$position.';top:'.$top.';bottom:'.$bottom.';right:'.$right.';left:'.$left.';" alt="img" class="img-responsive respimg"/>';	
				$html .='</div>';	
			} elseif ($featyretype == "st3"){	
			    $html .='<div class="sec-image post-media '.$class.'">';	
				if($link_url != '') {	
			        $html .='<a class="img-link" href="'.$link_url.'" target="'.$link_target.'">';
				}	
				    $html .='<img src="'.$cooper_back_image[0].'" style="width:'.$width.';height:'.$height.';float:'.$float.';margin:'.$margin.';padding:'.$padding.';position:'.$position.';top:'.$top.';bottom:'.$bottom.';right:'.$right.';left:'.$left.';" alt="img" class="img-responsive respimg"/>';
				if($link_url != '') {
			        $html .='</a>';
			    }	
				$html .='</div>';			

			} else {
			    $html .='<div class="sec-image post-media '.$class.'">';	
				    $html .='<img src="'.$cooper_back_image[0].'" style="width:'.$width.';height:'.$height.';float:'.$float.';margin:'.$margin.';padding:'.$padding.';position:'.$position.';top:'.$top.';bottom:'.$bottom.';right:'.$right.';left:'.$left.';" alt="img" class="img-responsive respimg"/>';
				$html .='</div>';	
			}		
                
		return $html;
	}
	add_shortcode('wr_vc_section_image', 'wr_vc_section_image_shortcode');
}


// Section Button Shortcode (Visual)

if(! function_exists('wr_vc_section_button_shortcode')){
	function wr_vc_section_button_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'title'=>'',
			'link_url'=>'',
			'link_target'=>'',
			'button_style'=>'',
			'margin'=>'',
			'padding'=>'',			
			'background'=>'',			
			'color'=>'',
			'border'=>'',
			'border_radius'=>'',		
			'font_size'=>'',
			'font_weight'=>'',
			'float'=>'',
			'icon_name'=>'',
			'text_transform'=>'',
			), $atts) );

				
		return '
		     <div class="sec-button '.$class.'">
			 <a class="btn hide-icon" href="'.$link_url.'" target="'.$link_target.'" style="margin:'.$margin.';padding:'.$padding.'; background:'.$background.'; color:'.$color.'; border:'.$border.'; border-radius:'.$border_radius.'; font-size:'.$font_size.'; font-weight:'.$font_weight.'; float:'.$float.';text-transform:'.$text_transform.';"><i class="fa '.$icon_name.'"></i><span>'.$title.'</span></a>
			 </div>
                
				';
	}
	add_shortcode('wr_vc_section_button', 'wr_vc_section_button_shortcode');
}


// Divider Section Shortcode (Visual)
if(! function_exists('wr_vc_bar_shortcode')){
	function wr_vc_bar_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'width'=>'',
			'height'=>'',
			'float'=>'',
			'color'=>'',
			'position'=>'',
			'margin'=>'',
			'padding'=>'',					
			'border'=>'',					
			'border_radius'=>'',
			
			), $atts) );

				
		return '
		    

            <div class="'.$class.'" style="width:'.$width.';height:'.$height.';float:'.$float.'; background:'.$color.'; position:'.$position.'; margin:'.$margin.'; padding:'.$padding.'; border:'.$border.'; border-radius:'.$border_radius.';"></div>			
			
				';
	}
	add_shortcode('wr_vc_bar', 'wr_vc_bar_shortcode');
}

// Blog Section Shortcode (Visual)

if(! function_exists('wr_vc_blog_shortcode')){
	function wr_vc_blog_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'title'=>'',
            'categoryname'=>'',
            'showpost'=>'',			
            'featyretype'=>'',			

			), $atts) );
			
			$html='';
			$cooper_options = get_option('cooper_wp');
			$html .='<div class="vc-blog-list '.$class.'">';
			
				if($featyretype == "st2"){
					$html .='<div class="searh-holder fl-wrap">';
						$html .='<div class="searh-inner">';
							$html .='<form id="searchfrom" role="search" method="get" action="';
							$html .= get_home_url('/');
							$html .='">';
								$html .='<input name="s" type="text" class="search" placeholder="Search.." />';
								$html .='<button class="search-submit" id="submit_btn"><i class="fa fa-search transition"></i> </button>';
							$html .='</form>';
						$html .='</div>';
					$html .='</div>';					
				}	
			
				global $post;
				$paged=(get_query_var('paged'))?get_query_var('paged'):1;
				$loop = new WP_Query( array( 'post_type' => 'post','category_name'=> $categoryname, 'posts_per_page'=> $showpost) );
				while ( $loop->have_posts() ) : $loop->the_post();
				
				$html .='<article class="post">';
				
					if (has_post_thumbnail( $post->ID ) ):
					$html .='<div class="post-media">';
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' );
					$html .='<a href="';
					$html .= get_the_permalink();
					$html .='">';	
					$html .='<img src="';
					$html .= $image[0];
					$html .= '" alt="" class="respimg"/>';			
					$html .='</a>';	
					$html .='</div>';           			
					
					elseif( has_post_format( 'video' ) !='') :
					if (get_post_meta($post->ID,'rnr_bl-video',true)!=''):;
					
					$html .='<div class="post-media">';
						$html .='<div class="iframe-holder">';
							$html .='<div class="resp-video cus-blog">';
								$html .='<iframe src="';
									$html .= get_post_meta($post->ID,'rnr_bl-video',true);
								$html .= '" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';							
							$html .='</div>';
						$html .='</div>';
					$html .='</div>';
					endif;
					
					elseif( has_post_format( 'audio' ) !='') :
					if (get_post_meta($post->ID,'rnr_bl-audio',true)!=''):;			
					
						$html .='<div class="iframe-holder">';
							$html .='<div class="post-media">';
								$html .='<div class="resp-video cus-blog">';
									$html .='<iframe src="';
										$html .= get_post_meta($post->ID,'rnr_bl-audio',true);
									$html .= '" width="100%" scrolling="no" frameborder="no"></iframe>';							
								$html .='</div>';
							$html .='</div>';
						$html .='</div>';
					endif;			
					
					elseif( has_post_format( 'gallery' ) !='') :
					
					$html .='<div class="post-media">';
						$html .='<div class="single-slider-holder fl-wrap lightgallery">';
							$html .='<div class="single-slider fl-wrap" data-loppsli="0">';
							
								$images = rwmb_meta( 'rnr_blog-image','type=image&size=cooper-portfolio-slider' );
								
								foreach ( $images as $image ){							
								$html .='<div class="item">';
									$html .='<img src="';
										 $html .= $image['url'];
									$html .= '" class="respimg"/>';			
									$html .= '<a data-src="';	 
										$html .= $image['url'];
									$html .= '" class="popup-image slider-zoom">
											<i class="fa fa-expand"></i>
											</a>';			
								$html .= '</div>';			
								}
									
							$html .='</div>';
							$html .='<div class="customNavigation gals">';
								$html .='<a class="prev-slide transition"> <i class="fa fa-angle-left"></i></a>';
								$html .='<a class="next-slide transition"><i class="fa fa-angle-right"></i></a>';
							$html .='</div>';						
						$html .='</div>';			
					$html .='</div>';
					
					endif;	

					$html .='<div class="post-item fl-wrap">';
						$html .='<h4><a href="';
						$html .= get_the_permalink();
						$html .='">';			
						$html .=get_the_title();
						$html .='</a></h4>';
					
						$html .='<ul class="post-meta">';
							$html .='<li class="author"><i class="fa fa-user"></i> ';
							$html .=get_the_author();
							$html .='</li>';
							$html .='<li><i class="fa fa-calendar-o"></i>';
							$html .=get_the_date( get_option( 'date_format' ) );
							$html .='</li>';
							$html .='<li><i class="fa fa-comments"></i> ';			
							$commentscount = get_comments_number();	
							if($commentscount == 1): $commenttext = 'Comment'; endif;
							if($commentscount == 0): $commenttext = 'Comment'; endif;
							if($commentscount > 1): $commenttext = 'Comments'; endif;
							$html .=$commentscount.' '.$commenttext;			
							$html .='</li>';			
							$html .='<li><i class="fa fa-folder-open-o"></i> ';
							$html .=get_the_category_list(', ');	
							$html .='</li>';
						$html .='</ul>';			
						
						$html .='<p>';	
						$html .= substr(strip_tags($post->post_content), 0, 280);
						$html .='...';	
						$html .='</p>';	
						
						if(!empty($cooper_options['blog-read-more'])):;
						$html .='<a href="';
						$html .= get_the_permalink();
						$html .='" class="btn hide-icon"><i class="fa fa-eye"></i><span>';			
						$html .= ($cooper_options['blog-read-more']);
						$html .='</span></a>';	
						else :
						$html .='<a href="';
						$html .= get_the_permalink();
						$html .='" class="btn hide-icon"><i class="fa fa-eye"></i><span>';			
						$html .='Continue reading';	
						$html .='</span></a>';	
						endif;
						
						$html .='<ul class="post-tags">';               
						$html .=get_the_tag_list('<li>',', ','<li>');               
						$html .='</ul>';
						
						if (( get_post_meta($post->ID,'rnr_blog_icon',true))):
						$html .='<div class="artcicle-icon"><i class="fa ';
						$html .= get_post_meta($post->ID,'rnr_blog_icon',true);
						$html .= '"></i></div>';
						else :	
						$html .= '<div class="artcicle-icon"><i class="fa fa-camera-retro"></i></div>';
						endif;			
				    $html .='</div>';
				$html .='</article>';
				
				endwhile;
				wp_reset_query();
			$html .='</div>';
							
		return $html;
		         
				
	}
	add_shortcode('wr_vc_blog', 'wr_vc_blog_shortcode');
}

// Protfolio Section Shortcode (Visual)

 if(! function_exists('wr_vc_portfolio_shortcode')){
	function wr_vc_portfolio_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			
			), $atts) );
			
		$html='';		
		
		global $post;
		$cooper_options = get_option('cooper_wp');
		
        $html .= '<div class="port-vc '.$class.'">';
		
		if(!get_post_meta(get_the_ID(), 'portfolio_category', true)):
		$portfolio_category = get_terms('portfolio_category');
		if($portfolio_category):
		$html .= '<div class="filter-wrap fl-wrap inline-filter">';
		$html .= '<div class="filter-button" style="display:none">Filter</div>';
		$html .= '<div class="gallery-filters">';
		
		if(!empty($cooper_options['prf-project-filter-all'])):
		$html .= '<a href="#" class="gallery-filter  gallery-filter-active" data-filter="*">';
		$html .= ($cooper_options['prf-project-filter-all']);
		$html .= '</a>';	
		else :
		$html .= '<a href="#" class="gallery-filter  gallery-filter-active" data-filter="*">All</a>';	
		endif;		
		
		foreach($portfolio_category as $portfolio_cat):
		$html .= '<a href="#" class="gallery-filter" data-filter=".';
		$html .= $portfolio_cat->slug;
		$html .= '">';
		$html .= $portfolio_cat->name;
		$html .= '</a>';
		endforeach;
		$html .= '</div>';
		$html .= '<div class="folio-counter">';
		$html .= '<div class="num-album"></div>';
		$html .= '<div class="all-album"></div>';
		$html .= '</div>';
		$html .= '</div>';
		endif; 
		endif;
        
		$html .= do_shortcode('[wr_vc_portfolio]');
		
		$html .= '</div>';
				
		return $html;
	}
	add_shortcode('wr_vc_portfolio_header', 'wr_vc_portfolio_shortcode');
}

 if(! function_exists('wr_vc_portfolio_body_shortcode')){
	function wr_vc_portfolio_body_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'title'=>'',
			'showpost'=>'',
			
			), $atts) );
		$html='';
		
		
		global $post;
		
		$html .= '<div class="gallery-items spad ">';
		
		query_posts(array('post_type' => 'portfolio','posts_per_page' => '-1'));
		while ( have_posts() ) : the_post();
		$portfolio_category = wp_get_post_terms($post->ID,'portfolio_category');
        $firenze_class = ""; 
		$firenze_categories = ""; 
		foreach ($portfolio_category as $firenze_item) {
			$firenze_class.=esc_attr($firenze_item->slug . ' ');
			$firenze_categories.='<span class="cat-divider">';
			$firenze_categories.=esc_attr($firenze_item->name . '  ');
			$firenze_categories.='</span>';
		}	
		
		if (( get_post_meta($post->ID,'rnr_port-img-size',true))=='full'){
		$html .= '<div class="gallery-item gallery-item-second ';
		$html .= $firenze_class;	
		$html .= ' ">';
		} elseif (( get_post_meta($post->ID,'rnr_port-img-size',true))=='inner'){
		$html .= '<div class="gallery-item uides ';
		$html .= $firenze_class;	
		$html .= ' ">';			
		} else {
		$html .= '<div class="gallery-item ';
		$html .= $firenze_class;	
		$html .= ' ">';			
		}
		
		$html .= '<div class="grid-item-holder">';
		$html .= '<div class="box-item vis-det folio-img fl-wrap">';

		if (has_post_thumbnail( $post->ID ) ):		
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' );
		$html .='<img src="';
		$html .= $image[0];
		$html .= '" alt="" />';				
		
		
		if (( get_post_meta($post->ID,'rnr_portfolio-post-formats',true))=='video'){
			
		if (( get_post_meta($post->ID,'rnr_portfolio-video-player',true))=='yes'):	
		$html .= '<a href="https://www.youtube.com/watch?v=';
		$html .= get_post_meta($post->ID,'rnr_portfolio-video',true);
		$html .= '" class="image-popup"><i class="fa fa-play-circle"></i></a>';	
		else :
		$html .= '<a href="https://vimeo.com/';
		$html .= get_post_meta($post->ID,'rnr_portfolio-video',true);
		$html .= '" class="image-popup"><i class="fa fa-play-circle"></i></a>';
		endif;
		
		} else {
		$html .= '<a data-src="';
		$html .= $image[0];
		$html .= '" class="image-popup"><i class="fa fa-search"></i></a>';
		}
		
		endif;		
		$html .= '</div>';	
		
		$html .= '<div class="grid-det fl-wrap">';	
		$html .= '<h3><a href="';
		$html .= get_the_permalink();
		$html .= '">';
		$html .=get_the_title();
		$html .= '</a></h3>';

		$html .= '<span class="prf-in-cat ">';
		$html .= $firenze_categories;
		$html .= '</span>';
		
		if (( get_post_meta($post->ID,'rnr_port_icon',true))):
		$html .='<i class="fa ';
		$html .= get_post_meta($post->ID,'rnr_port_icon',true);
		$html .= '"></i>';
        else :	
		$html .= '<i class="fa fa-camera-retro"></i>';
		endif;
		
		$html .= '</div>';

		if (( get_post_meta($post->ID,'rnr_port-img-size',true))=='full'){
		$html .= '</div>';
		} elseif (( get_post_meta($post->ID,'rnr_port-img-size',true))=='inner'){
		$html .= '</div>';		
		} else {
		$html .= '</div>';		
		}		

        $html .= '</div>';		
		
		endwhile;
		wp_reset_query();
		
		$html .= '</div>';
		
				
		return $html;
	}
	add_shortcode('wr_vc_portfolio', 'wr_vc_portfolio_body_shortcode');
}


// Resume Shortcode (Visual)

if(! function_exists('wr_vc_resume_shortcode')){
	function wr_vc_resume_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'showpost'=>'',	
			'categoryname'=>''									
			), $atts) );
		$html='';		
		$html .='<div class="sec-resume '.$class.'">';		
		    $html .='<div class="custom-inner-holder">';
				global $post;
				$paged=(get_query_var('paged'))?get_query_var('paged'):1;
				$loop = new WP_Query( array( 'post_type' => 'resume','resume_category'=> $categoryname,'posts_per_page'=> $showpost) );
				while ( $loop->have_posts() ) : $loop->the_post();				
				$html .='<div class="custom-inner">';
					$html .='<div class="row">';						
						$html .='<div class="col-md-4">';
							$html .='<div class="resum-header workres">';
								if (get_post_meta($post->ID,'rnr_resume_icon',true)!=''):;
								$html .='<i class="fa ';
								$html .= get_post_meta($post->ID,'rnr_resume_icon',true);
								$html .='"></i>';
								endif;					
								$html .='<h3>';
								$html .=get_the_title();
								$html .='</h3>';
								if (get_post_meta($post->ID,'rnr_resume_time',true)!=''):;	
								$html .='<span> ';
								$html .= get_post_meta($post->ID,'rnr_resume_time',true);
								$html .=' </span>';
								endif;	
							$html .='</div>';				
						$html .='</div>';						
						$html .='<div class="col-md-8">';
		
							$html .='<p>';
							$html .=get_the_content();
							$html .='</p>';		
				
							$html .='<span class="custom-inner-dec"></span>';
						$html .='</div>';
					$html .='</div>';
				$html .='</div>';				
				endwhile;
				wp_reset_query();
		    $html .='</div>';
		$html .='</div>';				
		return $html;
	}
	add_shortcode('wr_vc_resume', 'wr_vc_resume_shortcode');
}

// Services Shortcode (Visual)

if(! function_exists('wr_vc_services_shortcode')){
	function wr_vc_services_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'showpost'=>'',	
			'categoryname'=>'',						
			'show_hide'=>'',						
												
			), $atts) );
			
		$html='';
		global $post, $post_id, $values;
		$html .='<div class="sec-services '.$class.'">';
		    $html .='<div class="fl-wrap serv-wrap">';
		        $html .='<div class="row">';
		            $html .='<div class="col-md-3">';		
				        $html .= '<ul class="tabs sl-tabs">';
						$paged=(get_query_var('paged'))?get_query_var('paged'):1;
						$loop = new WP_Query( array( 'post_type' => 'services','services_category'=> $categoryname,'posts_per_page'=> $showpost) ); while ( $loop->have_posts() ) : $loop->the_post();
						    if (( get_post_meta($post->ID,'rnr_services_selected',true))=='yes'):
							$html .= '<li class="tab-link current" data-tab="';	
								$html .=get_the_ID();
							$html .= '">';
								$html .= '<div class="tb-item">';
									if (get_post_meta($post->ID,'rnr_services_icon',true)!=''):;
									$html .='<i class="fa ';
									$html .= get_post_meta($post->ID,'rnr_services_icon',true);
									$html .='"></i>';
									endif;	
									$html .='<h3>';
									$html .=get_the_title();
									$html .='</h3>';									
								$html .='</div>';									
							$html .= '</li>';
							else :
							$html .= '<li class="tab-link" data-tab="';	
								$html .=get_the_ID();
							$html .= '">';
								$html .= '<div class="tb-item">';
									if (get_post_meta($post->ID,'rnr_services_icon',true)!=''):;
									$html .='<i class="fa ';
									$html .= get_post_meta($post->ID,'rnr_services_icon',true);
									$html .='"></i>';
									endif;	
									$html .='<h3>';
									$html .=get_the_title();
									$html .='</h3>';									
								$html .='</div>';									
							$html .= '</li>';
							endif;		
						endwhile;
						wp_reset_query();
				        $html .= '</ul>';		
				    $html .= '</div>';		
		
		            $html .='<div class="col-md-9">';
					$paged=(get_query_var('paged'))?get_query_var('paged'):1;
					$loop = new WP_Query( array( 'post_type' => 'services','services_category'=> $categoryname,'posts_per_page'=> $showpost) ); while ( $loop->have_posts() ) : $loop->the_post();
						if (( get_post_meta($post->ID,'rnr_services_selected',true))=='yes'):
						$html .= '<div class="tab-content current" id="';	
							$html .=get_the_ID();
						$html .= '">';		
							$html .='<div class="row">';
								$html .='<div class="col-md-7">';
									if (has_post_thumbnail( $post->ID ) ):	
									$html .='<div class="box-item vis-det fl-wrap">';
										$cooper_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' );
										$html .= '<img src="';
											$html .= $cooper_image[0];
										$html .= '" alt="" class="respimg"/>';									
										$cooper_image2 = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' );	
										$html .='<a class="image-popup" data-src="';
											$html .= $cooper_image2[0];
										$html .='"><i class="fa fa-search"></i></a>';
									$html .= '</div>';	
									endif;	
								$html .= '</div>';	
								$html .='<div class="col-md-5">';
									$html .='<ul class="dec-list">';
										$values =  rwmb_meta('rnr_services_subtitle', $args = array('type'=>'text',),
										$post_id = $post->ID); 
										if($values){foreach ((array) $values as $key => $value) {
										$html .= '<li>';
										$html .= $value;
										$html .= '</li>';
										}}; 		
									$html .='</ul>';
									if (get_post_meta($post->ID,'rnr_services_price',true)!=''):;
									$html .='<span class="price">';
									$html .= get_post_meta($post->ID,'rnr_services_price',true);
									$html .='</span>';
									endif;										
								$html .= '</div>';										
							$html .='</div>';
							$html .='<h3>';
							$html .=get_the_title();
							$html .='</h3>';				
							$html .='<p>';
							$html .=get_the_content();
							$html .='</p>';									
						$html .='</div>';
						else :
						$html .= '<div class="tab-content" id="';	
							$html .=get_the_ID();
						$html .= '">';		
							$html .='<div class="row">';
								$html .='<div class="col-md-7">';
									if (has_post_thumbnail( $post->ID ) ):	
									$html .='<div class="box-item vis-det fl-wrap">';
										$cooper_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' );
										$html .= '<img src="';
											$html .= $cooper_image[0];
										$html .= '" alt="" class="respimg"/>';									
										$cooper_image2 = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' );	
										$html .='<a class="image-popup" data-src="';
											$html .= $cooper_image2[0];
										$html .='"><i class="fa fa-search"></i></a>';
									$html .= '</div>';	
									endif;	
								$html .= '</div>';	
								$html .='<div class="col-md-5">';
									$html .='<ul class="dec-list">';
										$values =  rwmb_meta('rnr_services_subtitle', $args = array('type'=>'text',),
										$post_id = $post->ID); 
										if($values){foreach ((array) $values as $key => $value) {
										$html .= '<li>';
										$html .= $value;
										$html .= '</li>';
										}}; 		
									$html .='</ul>';
									if (get_post_meta($post->ID,'rnr_services_price',true)!=''):;
									$html .='<span class="price">';
									$html .= get_post_meta($post->ID,'rnr_services_price',true);
									$html .='</span>';
									endif;										
								$html .= '</div>';										
							$html .='</div>';
							$html .='<h3>';
							$html .=get_the_title();
							$html .='</h3>';				
							$html .='<p>';
							$html .=get_the_content();
							$html .='</p>';									
						$html .='</div>';					
						endif;		

					endwhile;
					wp_reset_postdata();
					$html .='</div>';
		        $html .='</div>';
		    $html .='</div>';
		$html .='</div>';
				
		return $html;
	}
	add_shortcode('wr_vc_services', 'wr_vc_services_shortcode');
}

// Testimonials Shortcode (Visual)

if(! function_exists('wr_vc_testimonial_shortcode')){
	function wr_vc_testimonial_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'showpost'=>'',	
			'categoryname'=>'',						
			'show_hide'=>'',						
			
			), $atts) );

		$html='';
		
		$html .='<div class="sec-testimonial '.$class.'">';
		
		$html .='<div class="testimonials-slider-holder fl-wrap">';
		
		$html .='<div class="testimonials-slider owl-carousel">';		

		global $post;
		$paged=(get_query_var('paged'))?get_query_var('paged'):1;
		$loop = new WP_Query( array( 'post_type' => 'testimonials','testimonials_category'=> $categoryname,'posts_per_page'=> $showpost) );
 		while ( $loop->have_posts() ) : $loop->the_post();
		
		$html .='<div class="item">';
		$html .='<div class="testi-item">';

		if (has_post_thumbnail( $post->ID ) ):				
		$html .='<div class="testi-image">';
		$cooper_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'testimonial-img' );		
		$html .= '<img src="';
		$html .= $cooper_image[0];
		$html .= '" alt="" class="respimg" />';
		$html .= '</div>';
		endif;		
		
		$html .='<div class="testi-text fl-wrap">';		
		$html .='<h3>';
		$html .=get_the_title();
		$html .='</h3>';	

		$html .='<ul class="star-rating">';
			$values =  rwmb_meta('rnr_testi_rating', $args = array('type'=>'text',),
			$post_id = $post->ID); 
			if($values){foreach ((array) $values as $key => $value) {
			$html .= '<li>';
			$html .= $value;
			$html .= '</li>';
			}}; 		
		$html .='</ul>';
		
		$html .='<p>" ';
		$html .=get_the_content();
		$html .=' "</p>';
        if (get_post_meta($post->ID,'rnr_testimonial_linkname',true)!=''):;			
		$html .='<a href="';
        if (get_post_meta($post->ID,'rnr_testimonial_linkurl',true)!=''):;		
		$html .= get_post_meta($post->ID,'rnr_testimonial_linkurl',true);
		endif;		
		$html .='" class="testim-link" target="_blank">';	
		$html .= get_post_meta($post->ID,'rnr_testimonial_linkname',true);	
		$html .='</a>';	
		endif;						
		$html .='</div>';

		$html .='</div>';
		$html .='</div>';
		endwhile;
		wp_reset_postdata();

		$html .='</div>';
		
		$html .='<div class="customNavigation">';
		$html .='<a class="next-slide transition"><i class="fa fa-angle-right"></i></a>';
		$html .='<a class="prev-slide transition"><i class="fa fa-angle-left"></i></a>';
		$html .='</div>';	
		$html .='<div class="teti-counter"></div>';	
		
		$html .='</div>';
		$html .='</div>';
				
		return $html;
	}
	add_shortcode('wr_vc_testimonial', 'wr_vc_testimonial_shortcode');
}

// Progress Bar Section Shortcode (Visual)
if(! function_exists('wr_vc_progressbar_shortcode')){
	function wr_vc_progressbar_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'title'=>'',
			'counter_num'=>'',							
			'padex'=>'',							
			),  $atts) );

		$html='';

		$html .='<div class="sec-progress '.$class.' '.$padex.'">';
			$html .='<div class="fl-wrap">';
			    $html .='<div class="skillbar-box animaper">';
				    $html .='<div class="custom-skillbar-title"><span>'.$title.'</span></div>';
				    $html .='<div class="skill-bar-percent">'.$counter_num.'%</div>';
				    $html .='<div class="skillbar-bg" data-percent="'.$counter_num.'%">';           
			            $html .='<div class="custom-skillbar"></div>';
			        $html .='</div>';
			    $html .='</div>';
			$html .='</div>';
		$html .='</div>';
        
        return $html;		
				
	}
	add_shortcode('wr_vc_progress_bar', 'wr_vc_progressbar_shortcode');
}

// Counter Section Shortcode (Visual)

if(! function_exists('wr_vc_counter_shortcode')){
	function wr_vc_counter_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'icon1'=>'',
			'icon2'=>'',
			'icon3'=>'',
			'icon4'=>'',
			'counter_name1'=>'',			
			'counter_num1'=>'',		
			'counter_name2'=>'',			
			'counter_num2'=>'',	
			'counter_name3'=>'',			
			'counter_num3'=>'',		
			'counter_name4'=>'',			
			'counter_num4'=>'',				
			), $atts) );

				
		$html='';
		    
			$html .='<div class="sec-counter '.$class.'">';			

                $html .='<div class="inline-facts-holder fl-wrap">';
                    
                    if($counter_num1 != '') {                    
                    $html .='<div class="inline-facts">';
                        $html .='<div class="milestone-counter">';
                            $html .='<div class="stats animaper">';
                                $html .='<div class="num" data-content="'.$counter_num1.'" data-num="'.$counter_num1.'">0</div>';
                            $html .='</div>';
                        $html .='</div>';
                        $html .='<h6>'.$counter_name1.'</h6>';
                        $html .='<i class="fa '.$icon1.'" aria-hidden="true"></i>';
                    $html .='</div>';
                    } if($counter_num2 != '') {                   
                    $html .='<div class="inline-facts">';
                        $html .='<div class="milestone-counter">';
                            $html .='<div class="stats animaper">';
                                $html .='<div class="num" data-content="'.$counter_num2.'" data-num="'.$counter_num2.'">0</div>';
                            $html .='</div>';
                        $html .='</div>';
                        $html .='<h6>'.$counter_name2.'</h6>';
						$html .='<i class="fa '.$icon2.'" aria-hidden="true"></i>';
                    $html .='</div>';
                    } if($counter_num3 != '') {                     
                    $html .='<div class="inline-facts">';
                        $html .='<div class="milestone-counter">';
                            $html .='<div class="stats animaper">';
                                $html .='<div class="num" data-content="'.$counter_num3.'" data-num="'.$counter_num3.'">0</div>';
                            $html .='</div>';
                        $html .='</div>';
                        $html .='<h6>'.$counter_name3.'</h6>';
						$html .='<i class="fa '.$icon3.'" aria-hidden="true"></i>';
                    $html .='</div>';
					} if($counter_num4 != '') {                     
                    $html .='<div class="inline-facts">';
                        $html .='<div class="milestone-counter">';
                            $html .='<div class="stats animaper">';
                                $html .='<div class="num" data-content="'.$counter_num4.'" data-num="'.$counter_num4.'">0</div>';
                            $html .='</div>';
                        $html .='</div>';
                        $html .='<h6>'.$counter_name4.'</h6>';
						$html .='<i class="fa '.$icon4.'" aria-hidden="true"></i>';
                    $html .='</div>';
					}										
				$html .='</div>';	
            $html .='</div>';
                
		return $html;
	}
	add_shortcode('wr_vc_counter', 'wr_vc_counter_shortcode');
}
// Action Button Section Shortcode (Visual)
if(! function_exists('wr_vc_action_shortcode')){
	function wr_vc_action_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'img_url'=>'',
			'title'=>'',
			'title2'=>'',
			'title_color'=>'',																			
			'link_text'=>'',						
			'link_url'=>'',						
			'link_target'=>'',	
			'button_color'=>'',				
			), $atts) );
        $html='';		
		    $html .='<div class="sec-action '.$class.'">';  
                $html .='<div class="clearfix"></div>'; 
				$html .='<div class="order-wrap fl-wrap color-bg">'; 
					$html .='<div class="row">'; 
						if($title != '') {  
						$html .='<div class="col-md-8">'; 								
							$html .='<h4 style="color:'.$title_color.';">'.$title.'</h4>';
						$html .='</div>';	
						} if($link_text != '') { 
						$html .='<div class="col-md-4">';	
							$html .='<a href="'.$link_url.'" target="'.$link_target.'" style="color:'.$button_color.';" class="ord-link">'.$link_text.'</a>';
						$html .='</div>';	
						}
					$html .='</div>';	     				
				$html .='</div>';	     				
            $html .='</div>';  		
			
		return $html;
	}
	add_shortcode('wr_vc_action', 'wr_vc_action_shortcode');
}
// Social Icon (Visual)

if(! function_exists('wr_vc_social_shortcode')){
	function wr_vc_social_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',	
            'float'=>'',			

			), $atts) );

		$html ='';
		$cooper_options = get_option('cooper_wp');
		$html .= '<div class="sec-social-icon row '.$class.'">';
		$html .= '<div class="col-md-6">';
		$html .= '<div class="contact-social">';
		
		$html .= '<ul>';
		
		if(!empty($cooper_options['facebook'])):;
		$html .= '<li>';
		$html .= '<a href="';
		$html .= ($cooper_options['facebook']);
		$html .='" target="_blank">';	
		$html .= '<i class="fa fa-facebook"></i>';
		$html .= '</a>';
		$html .= '</li>';
		endif;
		
		if(!empty($cooper_options['twitter'])):;
		$html .= '<li>';
		$html .= '<a href="';
		$html .= ($cooper_options['twitter']);
		$html .='" target="_blank">';		
		$html .= '<i class="fa fa-twitter"></i>';
		$html .= '</a>';
		$html .= '</li>';
		endif;	

		if(!empty($cooper_options['google-plus'])):;
		$html .= '<li>';
		$html .= '<a href="';
		$html .= ($cooper_options['google-plus']);
		$html .='" target="_blank">';			
		$html .= '<i class="fa fa-google-plus"></i>';
		$html .= '</a>';
		$html .= '</li>';
		endif;			

		if(!empty($cooper_options['linkedin'])):;
		$html .= '<li>';
		$html .= '<a href="';
		$html .= ($cooper_options['linkedin']);
		$html .='" target="_blank">';		
		$html .= '<i class="fa fa-linkedin"></i>';
		$html .= '</a>';
		$html .= '</li>';
		endif;	

		if(!empty($cooper_options['instagram'])):;
		$html .= '<li>';
		$html .= '<a href="';
		$html .= ($cooper_options['instagram']);
		$html .='" target="_blank">';		
		$html .= '<i class="fa fa-instagram"></i>';
		$html .= '</a>';
		$html .= '</li>';
		endif;

		if(!empty($cooper_options['pinterest'])):;
		$html .= '<li>';
		$html .= '<a href="';
		$html .= ($cooper_options['pinterest']);
		$html .='" target="_blank">';		
		$html .= '<i class="fa fa-pinterest-p"></i>';
		$html .= '</a>';
		$html .= '</li>';
		endif;

		if(!empty($cooper_options['whatsapp'])):;
		$html .= '<li>';
		$html .= '<a href="';
		$html .= ($cooper_options['whatsapp']);
		$html .='" target="_blank">';		
		$html .= '<i class="fa fa-whatsapp"></i>';
		$html .= '</a>';
		$html .= '</li>';
		endif;	

		if(!empty($cooper_options['skype'])):;
		$html .= '<li>';
		$html .= '<a href="';
		$html .= ($cooper_options['skype']);
		$html .='" target="_blank">';	
		$html .= '<i class="fa fa-skype"></i>';
		$html .= '</a>';
		$html .= '</li>';
		endif;	

		if(!empty($cooper_options['dribbble'])):;
		$html .= '<li>';
		$html .= '<a href="';
		$html .= ($cooper_options['dribbble']);
		$html .='" target="_blank">';		
		$html .= '<i class="fa fa-dribbble"></i>';
		$html .= '</a>';
		$html .= '</li>';
		endif;			
		
		if(!empty($cooper_options['youtube'])):;
		$html .= '<li>';
		$html .= '<a href="';
		$html .= ($cooper_options['youtube']);
		$html .='" target="_blank">';		
		$html .= '<i class="fa fa-youtube"></i>';
		$html .= '</a>';
		$html .= '</li>';
		endif;			

		if(!empty($cooper_options['vimeo'])):;
		$html .= '<li>';
		$html .= '<a href="';
		$html .= ($cooper_options['vimeo']);
		$html .='" target="_blank">';		
		$html .= '<i class="fa fa-vimeo"></i>';
		$html .= '</a>';
		$html .= '</li>';
		endif;	

		if(!empty($cooper_options['dropbox'])):;
		$html .= '<li>';
		$html .= '<a href="';
		$html .= ($cooper_options['dropbox']);
		$html .='" target="_blank">';
		$html .= '<i class="fa fa-dropbox"></i>';
		$html .= '</a>';
		$html .= '</li>';
		endif;	

		if(!empty($cooper_options['github'])):;
		$html .= '<li>';
		$html .= '<a href="';
		$html .= ($cooper_options['github']);
		$html .='" target="_blank">';			
		$html .= '<i class="fa fa-github"></i>';
		$html .= '</a>';
		$html .= '</li>';
		endif;			
										
		$html .= '</ul>';
		
		$html .= '</div>';
				
		return $html ;
	}
	add_shortcode('wr_vc_social_icon', 'wr_vc_social_shortcode');
}
// Contact Info (Visual)
if(! function_exists('wr_vc_contact_info_shortcode')){
	function wr_vc_contact_info_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'title'=>'',
			'con_phone'=>'',
			'con_address'=>'',
			'con_mail'=>'',
	

			), $atts) );

				
		return '
		
		<div class="sec-contact-info '.$class.'">
		    <div class="row">
			    <div class="col-md-6">
				    <div class="contact-info">
			            '.$content.'					
                    </div>
                 </div>
            </div>
        </div>
        
				';
	}
	add_shortcode('wr_vc_contact_info', 'wr_vc_contact_info_shortcode');
}

// Contact Form (Visual)
if(! function_exists('wr_vc_contact_shortcode')){
	function wr_vc_contact_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'title'=>'',
			'contactfromid'=>'',
			), $atts) );

				
		return '
                <h4 class="bold-title">'.$title.'</h4>
				<div class="fl-wrap">
					<div id="contact-form" class="'.$class.'"><div id="message"></div>
						'.do_shortcode('[contact-form-7 id="'.$contactfromid.'" title="Contact form"]').'
					</div>
                </div>
			';
	}
	add_shortcode('wr_vc_contact_form', 'wr_vc_contact_shortcode');
}
// Google Map (Visual)
if(! function_exists('wr_vc_map_shortcode')){
	function wr_vc_map_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			), $atts) );			
		$html ='';		
		$html .= '<div class="map-box box-map '.$class.'">
                      <div  id="map-canvas"></div>
                </div>';				
		return $html;		
	}
	add_shortcode('wr_vc_map', 'wr_vc_map_shortcode');
}
?>